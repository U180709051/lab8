package shapes2d;

public class Square {
    protected double length;

    public Square(double length) {
        this.length = length;
    }
    public double area(){
        return length*length;
    }

    @Override
    public String toString() {
        return "Square{" +
                "length=" + length +
                '}';
    }
}
