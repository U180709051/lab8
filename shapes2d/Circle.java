package shapes2d;

public class Circle {
    protected double radius;

    public Circle(double radius) {
        this.radius = radius;
    }
    public double area(){
        return this.radius*this.radius;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "radius=" + radius +
                '}';
    }
}
