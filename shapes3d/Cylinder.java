package shapes3d;

import shapes2d.Circle;

public class Cylinder extends Circle{
    private int length;
    public Cylinder(int radius,int length) {
        super(radius);
        this.length=length;
    }
    public double area(){
        return super.area()+2*radius*length*Math.PI;
        }
    public double volume(){
        return super.area()*length;
    }

    @Override
    public String toString() {
        return "Cylinder{" +
                "length=" + length +
                ", radius=" + radius +
                '}';
    }
}
