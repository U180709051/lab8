package shapes3d;

import shapes2d.Square;

public class Cube extends Square {
    public Cube(int length) {
        super(length);
    }
    public double area(){
        return 6*super.area();
    }
    public double volume(){
        return Math.pow(length,3);
    }

    @Override
    public String toString() {
        return "Cube{" +
                "length=" + length +
                '}';
    }
}
